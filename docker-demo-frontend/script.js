const apiUrl = 'http://localhost:5000/api/profile/me';
const token = localStorage.getItem('token');

function fetchUser() {
    fetch(apiUrl, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response.json())
    .then(data => {
        document.getElementById('email').textContent = data.email;
        document.getElementById('name').textContent = data.name;
        document.getElementById('bio').textContent = data.bio;
        document.getElementById('emailInput').value = data.email;
        document.getElementById('nameInput').value = data.name;
        document.getElementById('bioInput').value = data.bio;
    })
    .catch(error => console.error('Error fetching user:', error));
}

function editProfile() {
    document.getElementById('profile-section').style.display = 'none';
    document.getElementById('edit-section').style.display = 'block';
}

function saveProfile(event) {
    event.preventDefault();
    const email = document.getElementById('emailInput').value;
    const name = document.getElementById('nameInput').value;
    const bio = document.getElementById('bioInput').value;

    fetch(apiUrl, {
        method: 'PATCH',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email, name, bio })
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Error updating profile');
        }
        return response.json();
    })
    .then(data => {
        document.getElementById('email').textContent = data.email;
        document.getElementById('name').textContent = data.name;
        document.getElementById('bio').textContent = data.bio;
        document.getElementById('profile-section').style.display = 'block';
        document.getElementById('edit-section').style.display = 'none';
        document.getElementById('message').textContent = 'Profile updated successfully';
    })
    .catch(error => {
        console.error('Error updating profile:', error);
        document.getElementById('message').textContent = 'Error updating profile';
    });
}

document.getElementById('profileForm').addEventListener('submit', saveProfile);

fetchUser();

