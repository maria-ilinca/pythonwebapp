from pymongo import MongoClient
import bcrypt

client = MongoClient('mongodb://localhost:27017/')
db = client['your_database_name']
users_collection = db['users']

class User:
    @staticmethod
    def find_user_by_username(username):
        return users_collection.find_one({'username': username})

    @staticmethod
    def find_user_by_id(user_id):
        return users_collection.find_one({'_id': user_id})

    @staticmethod
    def create_user(username, password, email, name):
        hashed = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
        user_id = users_collection.insert_one({
            'username': username,
            'password': hashed,
            'email': email,
            'name': name,
            'bio': ''
        }).inserted_id
        return user_id

    @staticmethod
    def update_user(user_id, updates):
        users_collection.update_one({'_id': user_id}, {'$set': updates})

