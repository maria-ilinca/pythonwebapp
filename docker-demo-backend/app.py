from flask import Flask, request, jsonify
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity
from models import User
import bcrypt  # Ensure bcrypt is imported

app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = 'your_jwt_secret'
jwt = JWTManager(app)

@app.route('/api/auth/register', methods=['POST'])
def register():
    app.logger.debug('Register endpoint hit')
    print(request.method)  # Check the HTTP method
    data = request.get_json()
    print(data)
    username, password, email, name = data['username'], data['password'], data['email'], data['name']
    if User.find_user_by_username(username):
        return jsonify({'error': 'Username already exists'}), 400
    user_id = User.create_user(username, password, email, name)
    return jsonify({'message': 'User created', 'user_id': user_id}), 201

@app.route('/api/auth/login', methods=['POST'])
def login():
    app.logger.debug('Login endpoint hit')
    data = request.get_json()
    username, password = data['username'], data['password']
    user = User.find_user_by_username(username)
    if not user or not bcrypt.checkpw(password.encode('utf-8'), user['password']):
        return jsonify({'error': 'Invalid credentials'}), 400
    access_token = create_access_token(identity=str(user['_id']))
    return jsonify({'token': access_token}), 200

@app.route('/api/profile/me', methods=['GET', 'PATCH'])
@jwt_required()
def profile():
    app.logger.debug('Profile endpoint hit')
    user_id = get_jwt_identity()
    user = User.find_user_by_id(user_id)
    if request.method == 'GET':
        return jsonify({
            'username': user['username'],
            'email': user['email'],
            'name': user['name'],
            'bio': user['bio']
        })
    elif request.method == 'PATCH':
        data = request.get_json()
        allowed_updates = ['email', 'name', 'bio']
        updates = {key: data[key] for key in allowed_updates if key in data}
        User.update_user(user_id, updates)
        return jsonify({'message': 'Profile updated'})

@app.route('/')
def index():
    return jsonify({'message': 'Welcome to the API'})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
